%%  Metodo 3

[melodia fs] = audioread('all_star.wav');

f_min = 125;
f_max = 1000;
N_max = fix(fs/f_min);
N_min = fix(fs/f_max);

tam_ventana = 512;
L = fix(length(melodia)/tam_ventana); 

h    = fft([1 -15/16], tam_ventana); 


ham1 = load('ham512.mat').ham;

frecuencias1 = zeros(1, L);  
frecuencias2 = zeros(1, L);  

for i = 0:L-1
    
    ventana =  i*tam_ventana+1:(i+1)*tam_ventana;
    

    SMO = abs(fft(melodia(ventana).*ham1));
    SM  = abs(fft(melodia(ventana).*ham1));
    %SM  = abs(fft(melodia(ventana).*hamming(512)));
    SM  = SM(1:256);

    res = zeros(1, length(SM));

%   Detecci�n de Picos:
%   Se guardan los puntos con valores 
%   mayores a sus dos vecinos

    for k = 2:length(SM)-1
        if SM(k-1) < SM(k) && SM(k) > SM(k+1)
            res(k) = SM(k);             
        end      
    end

%   Guardar picos. En este caso 2.
%   En el caso de a�adir mas, el comportamiento empeora.

    picos = 2;
    maximos = [];
    for k = 1:picos
        [v j] = max(res);
        maximos = [maximos j];
        res(j) = 0;
    end

    
    maximos     = sort(maximos);
    diferencias = zeros(1, length(maximos)-1);

    for k = 1:length(maximos)-1
       diferencias(k) = maximos(k+1)-maximos(k); 
    end

    %   Resultados Metodo3

    d0a = mean(diferencias);
    f0a = d0a*fs/tam_ventana;

    %   Resultados Metodo2
    
    [f0b, d0b] = estimar_freqfund(SMO, f_min, f_max, fs, tam_ventana);
    
    %   Vectores de Resultados
    
    frecuencias1(i+1) = f0a;
    frecuencias2(i+1) = f0b;
    

end

%%

% Dibujar el resultado de la melod�a

figure, 
plot((1:tam_ventana:tam_ventana*L)/fs, frecuencias1);  % hold on;
% plot((1:tam_ventana:tam_ventana * L)/fs, frecuencias2, 'r'); hold off;
grid on; ylabel("Frecuencia (Hz)"); xlabel('Tiempo (s)');
title("Tonalidad de la melod�a"); 


%%

fase_ini = 0;
mus = ones(1, L*tam_ventana);

for k = 0:L-1
    
    fase = 2*pi*frecuencias1(k+1)*(1:tam_ventana)/fs+fase_ini;
    mus(k*tam_ventana+1:(k+1)*tam_ventana) = sin(fase);
    fase_ini = fase(end);
end

soundsc(mus, fs)
%%
audiowrite("all_star-joseba.wav", mus, fs);
