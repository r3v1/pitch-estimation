function [f0, d] = estimar_freqfund(s, f_min, f_max, fs, tam_ventana)
    %% Frecuencia fundamental
    % Dada una señal, obtiene su frecuencia fundamental calculando la 
    % distancia entre los picos 
    %   s:  Señal de entrada
    
    % Posibles distancias entre picos
    picos = round(f_min*tam_ventana/fs):round(f_max*tam_ventana/fs);
    
    cuentas = zeros(1, length(picos));
    for i = 1:length(picos)
        % Calcular el número de veces en el que los picos encajen con una
        % diferencia de 'picos(i)'
        cuentas(picos(i)) = es_mayor(picos(i), abs(s), 3);
    end
    
    % Obtenemos el índice con mayor número de encajes
    [~, idx] = max(cuentas);
    
    % Calculamos la estimación de la frecuencia fundamental
    d = picos(picos==idx);
    f0 = d * fs/tam_ventana;
    f0 = f0 - f0 * f_min/f_max;    
end