function y = es_mayor(n, s, l)
    % Devuelve el número de veces que el punto dado 'n', repetido cada 'n'
    % muestras, sea el máximo de entre sus 2 puntos alrederor
    %   n: El punto a analizar
    %   s: Señal de entrada
    %   l: Número de picos que medir
    
    % Calcula el número de puntos alrededor
    offset = floor(n / 2);
    
    % Inicializa una matriz para almacenar los resultados
    son_mayores = zeros(floor(length(s)/n)-1, offset);
    for i = n:n:n*l
        for k = 1:offset
            % Guarda en la matriz si el valor de la señal es el mayor de
            % sus vecinos equidistantes
            if i + k < length(s) && i - k > 0
                son_mayores(i/n, k) = abs(s(i-k)) < abs(s(i)) && abs(s(i+k)) < abs(s(i));
            end
        end
    end
    
    % Devuelve el número 
    y = length(find(son_mayores==1));
end