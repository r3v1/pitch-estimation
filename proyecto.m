clear all;

archivo = 'melodias/melodia.wav';

%% CARGAR Y PREPROCESAR LA MELODÍA
[melodia, fs] = audioread(archivo);   % Cargar el audio
%soundsc(melodia, fs);                     % Escuchar la melodía

%% CALCULAR EL RANGO DE FRECUENCIAS DE LA MELODÍA
%
% Suponiendo que la melodía se encuentra en las tres escalas
% centrales del piano, el rango de frecuencias variaara entre
% 125Hz y 1000Hz
f_min = 125;
f_max = 1000;
N_max = fix(fs/f_min);
N_min = fix(fs/f_max);

% Deberemos incluir por lo menos el doble del tamaño de las muestras
% para que las frecuencias bajas las detecte al calcular el periodo
% mediante correlación
tam_ventana = 2 * N_max;
fragmentos = fix(length(melodia)/tam_ventana);  % Número de fragmentos totales
frecuencias = zeros(1, fragmentos) + 1;         % Array para almacenar

%% FILTRAR LA SEÑAL
%
% Definir el filtro de preénfasis H(z) = 1 - 15/16z^(-1)
% Se trata de un filtro que potencia progresivamente las frecuencias altas
h = fft([1 -15/16], tam_ventana);

for fragmento = 0:fragmentos-1
    % Obtener el vector ventana
    ventana = fragmento * tam_ventana + 1:(fragmento+1) * tam_ventana;
    mel = melodia(ventana);

    % Aplicar Hamming
    melH = fft(mel .* hamming(tam_ventana));

    % Aplicar el filtro de préenfasis al fragmento
    melF = melH .* h';
    %x_frec = 1:fs/tam_ventana:fs;

    %% ESTIMACIÓN DE LA FRECUENCIA FUNDAMENTAL
    % Con autocorrelación
    %rxx = xcorr(real(ifft(melF)));
    %[~, d] = max(rxx(tam_ventana + N_min + 1:tam_ventana+N_max));
    %d0 = (d + N_min - 1);
    %f0 = fs / d0;

    % Técnica inventada
    [f0, d0] = estimar_freqfund(melF, f_min, f_max, fs, tam_ventana);

    % Guardamos la estimación de la frecuencia fundamental de la ventana
    frecuencias(fragmento + 1) = f0;

    % Comparar las señales en el dominio frecuencial
    %figure;
    %subplot(4, 1, 1), plot(20*log10(abs(mel(1:N_max)))), grid, title("Fragmento original"), xlabel("Muestras"), ylabel("dB");
    %subplot(4, 1, 2), plot(20*log10(abs(melH(1:N_max)))), grid, title("Fragmento con Hamming"), xlabel("Muestras"), ylabel("dB");
    %subplot(4, 1, 3), plot(20*log10(abs(h(1:N_max)))), grid, title("Filtro"), xlabel("Muestras"), ylabel("dB");
    %subplot(4, 1, 4), plot(1:N_max, 20*log10(abs(melF(1:N_max)))), grid, title("Fragmento filtrado (f. estimada: " + f0 + "Hz)"), xlabel("Muestras"), ylabel("dB");
end

% Dibujar el resultado de la melodía
fig = figure;
plot((1:tam_ventana:tam_ventana * fragmentos)/fs, frecuencias);
grid on;
title("Tonalidad de la melodía"); ylabel("Frecuencia (Hz)"); xlabel('Tiempo (s)');
saveas(fig, "results/pitch-melodia-david.png");

%% GENERACIÓN DE LA MELODÍA
fase_ini = 0;
mus = zeros(1, fragmentos * tam_ventana) + 1;
for k = 0:fragmentos-1
 fase = 2 * pi * frecuencias(k+1) * (1:tam_ventana)/fs + fase_ini;
 mus(k * tam_ventana + 1:(k+1) * tam_ventana) = sin(fase);
 fase_ini = fase(end);
end
%soundsc(mus, fs);
audiowrite("results/melodia-david.wav", mus, fs);
